// Soal 1

console.log("==================================================");
console.log("First answer");
console.log("--------------------------------------------------");

console.log("LOOPING PERTAMA");
var counter = 2;
while (counter <= 20) {
    console.log(String(counter) + " - I love coding");
    counter += 2;
}

console.log("LOOPING KEDUA");
while (counter > 2) {
    counter -= 2;
    console.log(String(counter) + " - I will become a mobile developer");
}


// Soal 2

console.log("==================================================");
console.log("Second answer");
console.log("--------------------------------------------------");

for (var i = 1; i <= 20; i++) {
    if (i % 3 === 0 && i % 2 === 1) {
        console.log(String(i) + " - I Love Coding");
    } else if (i % 2 === 0) {
        console.log(String(i) + " - Berkualitas");
    } else {
        console.log(String(i) + " - Santai");
    }
}


// Soal 3

console.log("==================================================");
console.log("Third answer");
console.log("--------------------------------------------------");

var length = 8;
var width = 4;
var currChar = '';

for (var i = 0; i < width; i++) {
    for (var j = 0; j < length; j++) {
        currChar += '#';
    }
    console.log(currChar);
    currChar = '';
}


// Soal 4

console.log("==================================================");
console.log("Fourth answer");
console.log("--------------------------------------------------");

var width = 7;
var currChar = '';

for (var i = 0; i < width; i++) {
    for (var j = 0; j <= i; j++) {
        currChar += '#';
    }
    console.log(currChar);
    currChar = '';
}


// Soal 5

console.log("==================================================");
console.log("Fifth answer");
console.log("--------------------------------------------------");

var width = 8;
var currChar = '';

for (var i = 0; i < width; i++) {
    for (var j = 0; j < width; j++) {
        if (i % 2 === 0) {
            if (j % 2 === 0) {
                currChar += ' ';
            } else {
                currChar += '#';
            }
        } else {
            if (j % 2 === 0) {
                currChar += '#';
            } else {
                currChar += ' ';
            }
        }
    }
    console.log(currChar);
    currChar = '';
}