// Soal 1

console.log("==================================================");
console.log("First answer");
console.log("--------------------------------------------------");

function teriak() {
    return "Halo Sanbers!"
}
 
console.log(teriak()); // "Halo Sanbers!" 


// Soal 2

console.log("==================================================");
console.log("Second answer");
console.log("--------------------------------------------------");

function kalikan(num1, num2) {
    return Number(num1) * Number(num2)
}
 
var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48


// Soal 3

console.log("==================================================");
console.log("Third answer");
console.log("--------------------------------------------------");

var introduce = function (name, age, address, hobby) {
    return "Nama saya " + String(name) + ", umur saya " + String(age) + " tahun, saya di " + 
    String(address) + ", dan saya punya hobby yaitu " + String(hobby) + "!";
}
 
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat
// saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 

console.log("=======================END========================");