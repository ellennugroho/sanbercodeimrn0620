// Soal 1

console.log("==================================================");
console.log("First answer");

// Asumsi input pada kode, bukan input dari command-line
// Asumsi peran strictly match sampai ke huruf kapitalnya juga

var nama = "John";
var peran = "";

if (nama === "") {
    console.log("Nama harus diisi!");
} else {
    if (peran == "") {
        console.log("Halo " + nama + ", pilih peranmu untuk memulai game!");
    } else if (peran === "Penyihir" || peran === "Guard" || peran === "Werewolf") {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        if (peran === "Penyihir") {
            console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
        } else if (peran === "Guard") {
            console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
        } else if (peran === "Werewolf") {
            console.log("Halo " + peran + " " + nama + ", kamu akan memakan mangsa setiap malam!")
        }
    }
}


// Soal 2

console.log("==================================================");
console.log("Second answer");

// Asumsi input dalam kode, bukan dari command-line
// Validasi disesuaikan dengan soal (tidak mengecek apabila pada bulan 2 terdapat tanggal 30, dst)

var hari = 21; 
var bulan = 1; 
var tahun = 1945;

var validated = true;
var bulanString;

// Validasi
// Dipilih if-else karena lebih sesuai untuk konteks ini
if (hari < 1 || hari > 31) {
    console.log("Hari tidak valid")
    validated = false;
}

if (bulan < 1 || bulan > 12) {
    console.log("Bulan tidak valid")
    validated = false;
}

if (tahun < 1900 || tahun > 2200) {
    console.log("Tahun tidak valid")
    validated = false;
}

if (validated) {
    switch (bulan) {
        case 1: { bulanString = "Januari"; break; }
        case 2: { bulanString = "Februari"; break; }
        case 3: { bulanString = "Maret"; break; }
        case 4: { bulanString = "April"; break; }
        case 5: { bulanString = "Mei"; break; }
        case 6: { bulanString = "Juni"; break; }
        case 7: { bulanString = "Juli"; break; }
        case 8: { bulanString = "Agustus"; break; }
        case 9: { bulanString = "September"; break; }
        case 10: { bulanString = "Oktober"; break; }
        case 11: { bulanString = "November"; break; }
        case 12: { bulanString = "Desember"; break; }
        default: {  }
    }

    console.log(String(hari) + " " + bulanString + " " + String(tahun))
}