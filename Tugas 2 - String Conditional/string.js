// Soal 1

console.log("==================================================");
console.log("First answer");

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var firstAnswer = word.concat(' ', second, ' ', third, ' ',
fourth, ' ', fifth, ' ', sixth, ' ', seventh);
console.log(firstAnswer);


// Soal 2

console.log("==================================================");
console.log("Second answer");

var sentence = "I am going to be React Native Developer"; 

// Alternatif solusi supaya tidak nguli: bisa dilakukan for loop dari 
// array of string yg dihasilkan 
// Applicable untuk soal2 selanjutnya
//
// var splittedSentence = sentence.split(" ");
// var exampleFirstWord = sentence[0];
// var exampleSecondWord = sentence[2] + sentence[3];
// var thirdWord = splittedSentence[2];
// var fourthWord = splittedSentence[3];
// var fifthWord = splittedSentence[4];
// var sixthWord = splittedSentence[5];
// var seventhWord = splittedSentence[6];
// var eighthWord = splittedSentence[7];

var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);


// Soal 3

console.log("==================================================");
console.log("Third answer");

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substr(4, 10); // do your own! 
var thirdWord2 = sentence2.substr(15, 2); // do your own! 
var fourthWord2 = sentence2.substr(18, 2); // do your own! 
var fifthWord2 = sentence2.substr(21, 4); // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);


// Soal 4

console.log("==================================================");
console.log("Fourth answer");

var firstWordLength = exampleFirstWord2.length  
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord2 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord2 + ', with length: ' + secondWord2.length); 
console.log('Third Word: ' + thirdWord2 + ', with length: ' + thirdWord2.length); 
console.log('Fourth Word: ' + fourthWord2 + ', with length: ' + fourthWord2.length); 
console.log('Fifth Word: ' + fifthWord2 + ', with length: ' + fifthWord2.length); 