var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

function readBooks(time, books) {
    readBooksPromise(time, books[0])
        .then(function (sisaWaktu) {
            readBooks(sisaWaktu, books.slice(1))
        })
        .catch(function (sisaWaktu) {
            console.log("ups, sudah habis buku/waktunya")
        })
}

readBooks(10000, books)