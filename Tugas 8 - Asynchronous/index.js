// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function recc(time, books) {
    if (books.length == 0) {
        console.log('buku habis')
    } else if (time <= 0) {
        console.log('waktu habis')
    } else {
        readBooks(time, books[0], function(sisaWaktu) {
            if (sisaWaktu != time) {
                // karena kalau dilihat dari kode di callback.js, ketika waktu keburu habis sebelum selesai baca
                // waktu pada callback tetap waktu awal, bukan 0
                // sehingga harusnya sudah berhenti
                recc(sisaWaktu, books.slice(1))
            }
        })
    }
}

recc(1000, books)