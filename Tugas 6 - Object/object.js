// Soal 1

console.log("==================================================");
console.log("First answer");
console.log("--------------------------------------------------");

function arrayToObject(arr) {
    var myObject = {}
    var now = new Date()
    var thisYear = now.getFullYear()
    var count = 1

    for (var people of arr) {
        tempObject = {
            firstName: people[0],
            lastName: people[1],
            gender: people[2],
            age: people[3] && thisYear >= people[3] ? Number(thisYear) - Number(people[3]) : 'Invalid birth year',
        }
        myObject[count] = tempObject
        count += 1
    }

    count = 1

    for (let obj in myObject) {
        console.log(String(count) + ". " + myObject[obj]['firstName'] + " " + myObject[obj]['lastName'] + ": " + JSON.stringify(myObject[obj]))
        count += 1
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""


// Soal 2

console.log("==================================================");
console.log("Second answer");
console.log("--------------------------------------------------");

function shoppingTime(memberId, money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }

    if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }

    var currMoney = money

    var shopList = [
        ['Sepatu Stacattu', 1500000], 
        ['Baju Zoro', 500000], 
        ['Baju H&N', 250000], 
        ['Sweater Uniklooh', 175000],
        ['Casing Handphone', 50000]
    ]

    shopList = shopList.sort(function(a,b) {
        return b[1] - a[1]
    })

    itemBought = []

    for (var i = 0; i < shopList.length; i++ ) {
        if (currMoney < 50000) {
            break
        }
        if (shopList[i][1] <= currMoney) {
            itemBought.push(shopList[i][0])
            currMoney -= shopList[i][1]
        }
    }

    const result = {
        memberId: memberId,
        money: money,
        listPurchased: itemBought,
        changeMoney: currMoney
    }

    return result
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// Soal 3
// Asumsi tidak ada rute yang kebalik, misal dari D ke A

console.log("==================================================");
console.log("Third answer");
console.log("--------------------------------------------------");

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var result = []

    if (arrPenumpang.length == 0) {
        return []
    }

    for (const arr of arrPenumpang) {
        from = arr[1]
        to = arr[2]
        bayar = 0
        idx = 0
        curr = rute[idx]

        while (curr != from) {
            idx += 1
            curr = rute[idx]
        }

        while (curr != to) {
            idx += 1
            bayar += 2000
            curr = rute[idx]
        }

        tempObject = {
            penumpang: arr[0],
            naikDari: arr[1],
            tujuan: arr[2],
            bayar: bayar
        }

        result.push(tempObject)
    }
    return result
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]