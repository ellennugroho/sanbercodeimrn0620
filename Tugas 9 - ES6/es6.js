// Soal 1

console.log("==================================================");
console.log("First answer");
console.log("--------------------------------------------------");

var golden = () => {
    console.log("this is golden!!")
}

golden()

// Soal 2

console.log("==================================================");
console.log("Second answer");
console.log("--------------------------------------------------");

const newFunction = function literal(firstName, lastName){
    return {
        firstName,
        lastName,
        fullName() {
            console.log(firstName + " " + lastName)
            return 
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName() 


// Soal 3

console.log("==================================================");
console.log("Third answer");
console.log("--------------------------------------------------");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)


// Soal 4

console.log("==================================================");
console.log("Fourth answer");
console.log("--------------------------------------------------");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

//Driver Code
console.log(combined)


// Soal 5

console.log("==================================================");
console.log("Fifth answer");
console.log("--------------------------------------------------");

const planet = "earth"
const view = "glass"

let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor ` + 
    `incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before)