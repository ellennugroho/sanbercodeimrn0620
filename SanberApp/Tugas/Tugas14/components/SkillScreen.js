import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    FlatList,
    Dimensions
} from 'react-native';

import { MaterialCommunityIcons } from '@expo/vector-icons';
import { setCustomText } from 'react-native-global-props';
import Skill from './Skill'
import data from '../skillData.json'

export default class SkillScreen extends Component {
    
    // constructor(props) {
    //     super(props)
    //     this.state = {
    //         noteArray: [],
    //         noteText: '',
    //     }
    // }

    render() {
        return (
            <View style={styles.container}>
                
                <Image source={require('../../Tugas13/images/logo.png')} style={styles.iconImage}></Image>
                
                <View style={styles.accountContainer}>
                    <MaterialCommunityIcons style={styles.accountIcon} name="account-circle" size={Dimensions.get('window').width * 0.0938} color="#3EC6FF" />
                    <View style={styles.accountText}>
                        <Text style={{ fontSize: 12, lineHeight: 14, color: 'black' }}>Hai,</Text>
                        <Text style={{ fontSize: 16, lineHeight: 19 }}>Mukhlis Hanafi</Text>
                    </View>
                </View>

                <Text style={styles.skillTitle}>SKILL</Text>

                <View style={styles.category}>

                    <Text style={styles.categoryText}>Library / Framework</Text>
                    <Text style={styles.categoryText}>Bahasa Pemrograman</Text>
                    <Text style={styles.categoryText}>Teknologi</Text>

                </View>

                <FlatList
                    data={data.items}
                    renderItem={(skillset)=><Skill skillset={skillset.item} />}
                    keyExtractor={(item)=>item.id}

                />

            </View>
        )
    }

}

const customTextProps = { 
    style: { 
        fontFamily: 'Roboto',
        color: '#003366',
    }
}
setCustomText(customTextProps);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 14
    },
    iconImage: {
        width: 187.5,
        height: 51,
        marginBottom: 3,
        left: Dimensions.get('window').width - 187.5 - 14,
    },
    accountContainer: {
        flexDirection: 'row'
    },
    accountIcon: {
        marginRight: 11
    },
    skillTitle: {
        fontSize: 36,
        marginTop: 19,
        borderBottomColor: '#3EC6FF',
        borderBottomWidth: 4
    },
    category: {
        flexDirection: 'row'
    },
    categoryText: {
        flex: 1,
        fontWeight: 'bold',
        fontSize: 12,
        lineHeight: 14,
        marginHorizontal: 3,
        marginVertical: 10,
        textAlign: 'center',
        textAlignVertical: 'center',
        backgroundColor: '#B4E9FF',
        borderRadius: 8
    }
})