import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { setCustomText } from 'react-native-global-props';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';

export default class Skill extends Component {

    render() {
        
        let skillset = this.props.skillset;
        
        return (

            <View style={styles.container}>

                <View style={styles.skill}>
                    
                    <MaterialCommunityIcons size={75} name={skillset.iconName} color='#003366' style={styles.icon} />

                    <View style={styles.skillLabel}>

                        <Text style={styles.skillName}>{skillset.skillName}</Text>
                        <Text style={styles.skillCategory}>{skillset.categoryName}</Text>
                        <Text style={styles.skillPercentage}>{skillset.percentageProgress}</Text>

                    </View>

                    <MaterialIcons name="keyboard-arrow-right" size={56} color="#003366" style={styles.arrow}/>

                </View>

            </View>

        )
    }

}

const customTextProps = { 
    style: { 
        fontFamily: 'Roboto',
        color: '#003366',
    }
}
setCustomText(customTextProps);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 4
    },
    skill: {
        backgroundColor: '#B4E9FF',
        flexDirection: 'row',
        borderRadius: 8,
    },
    icon: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 5
    },
    skillLabel: {
        flex: 2,
        padding: 13
    },
    skillName: {
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: 28
    },
    skillCategory: {
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,
        color: '#3EC6FF'
    },
    skillPercentage: {
        fontWeight: 'bold',
        fontSize: 48,
        lineHeight: 56,
        color: 'white',
        textAlign: 'right'
    },
    arrow: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center'
    }
})