import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';

import { setCustomText } from 'react-native-global-props';

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            titleText: "Login",
            userNameLabel: "Username / Email",
            passwordLabel: "Password",
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../Tugas13/images/logo.png')} style={{ width: 375, height: 102, marginTop: 63, marginBottom: 70 }}></Image>
                <Text style={styles.titleText}>
                    {this.state.titleText}
                </Text>
                <View style={styles.fieldContainer}>
                    <Text style={styles.labelText}>
                        {this.state.userNameLabel}
                    </Text>
                    <TextInput style={styles.textInput} />
                    <Text style={styles.labelText}>
                        {this.state.passwordLabel}
                    </Text>
                    <TextInput style={styles.textInput} />
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity>
                        <Text style={styles.loginButton}>
                            Masuk
                        </Text>
                    </TouchableOpacity>
                    <Text style={{color:"#3EC6FF", fontSize:24, margin:16}}>
                        atau
                    </Text>
                    <TouchableOpacity>
                        <Text style={styles.signupButton}>
                            Daftar ?
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const customTextProps = { 
    style: { 
      fontFamily: 'Roboto'
    }
}
setCustomText(customTextProps);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        color: '#003366',
    },
    titleText: {
        fontSize: 24,
        marginBottom: 40,
        color: '#003366',
    },
    fieldContainer: {
        alignItems: 'flex-start',
        width: 'auto'
    },
    labelText: {
        fontSize: 16,
        marginBottom: 4,
        color: '#003366',
    },
    textInput: {
        alignItems: 'stretch',
        borderColor: '#003366',
        borderWidth: 1,
        width: 294,
        height: 48,
        marginBottom: 16,
        padding: 10
    },
    buttonContainer: {
        alignItems: 'center',
        marginTop: 28
    },
    loginButton: {
        borderRadius: 16,
        backgroundColor: "#3EC6FF",
        width: 140,
        height: 40,
        fontSize: 24,
        color: 'white',
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    signupButton: {
        borderRadius: 16,
        backgroundColor: "#003366",
        width: 140,
        height: 40,
        fontSize: 24,
        color: 'white',
        textAlign: 'center',
        textAlignVertical: 'center'
    },
})