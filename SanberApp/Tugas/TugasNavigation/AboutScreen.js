import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput,
    Dimensions
} from 'react-native';

import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import { setCustomText } from 'react-native-global-props';

export default class About extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.aboutMe}>
                    <Text style={styles.titleText}>
                        Tentang Saya
                    </Text>
                    <MaterialIcons style={styles.accountIcon} name="account-circle" size={200} color="#CACACA"/>
                    <Text style={styles.nameText}>
                        Mukhlis Hanafi
                    </Text>
                    <Text style={styles.jobText}>
                        React Native Developer
                    </Text>
                </View>
                <View style={styles.portfolio}>
                    <Text style={{borderBottomColor: '#003366', borderBottomWidth: 1, padding: 5, textAlign: 'left'}}>
                        Portofolio
                    </Text>
                    <View style={styles.portoBox}>
                        <View style={styles.portoElement}>
                            <MaterialCommunityIcons name="gitlab" size={40} color="#3EC6FF" style={{textAlign: 'center'}} />
                            <Text style={styles.gitText}>@mukhlish</Text>
                        </View>
                        <View style={styles.portoElement}>
                            <MaterialCommunityIcons name="github-circle" size={40} color="#3EC6FF" style={{textAlign: 'center'}} />
                            <Text style={styles.gitText}>@mukhlis-h</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.contactMe}>
                    <Text style={{borderBottomColor: '#003366', borderBottomWidth: 1, padding: 5, textAlign: 'left'}}>
                        Hubungi Saya
                    </Text>
                    <View style={styles.contactMeBox}>
                        <View style={styles.contactElement}>
                            <MaterialCommunityIcons name="facebook-box" size={40} color="#3EC6FF" style={styles.socialIcon} />
                            <Text style={styles.accountText}>mukhlis.hanafi</Text>
                        </View>
                        <View style={styles.contactElement}>
                            <MaterialCommunityIcons name="instagram" size={40} color="#3EC6FF" style={styles.socialIcon} />
                            <Text style={styles.accountText}>@mukhlis.hanafi</Text>
                        </View>
                        <View style={styles.contactElement}>
                            <MaterialCommunityIcons name="twitter" size={40} color="#3EC6FF" style={styles.socialIcon} />
                            <Text style={styles.accountText}>@mukhlish</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const customTextProps = { 
    style: { 
        fontFamily: 'Roboto',
        color: '#003366',
    }
}
setCustomText(customTextProps);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        color: '#003366',
    },
    aboutMe: {
        justifyContent: "center",
        alignItems: "center"
    },
    titleText: {
        fontWeight: 'bold',
        color: '#003366',
        fontSize: 36,
        marginTop: 64,
    },
    accountIcon: {
        margin: 12
    },
    nameText: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    jobText: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#3EC6FF'
    },
    portfolio: {
        width: Dimensions.get('window').width - 8,
        height: 'auto',
        backgroundColor: "#EFEFEF",
        borderRadius: 16,
        marginTop: 16
    },
    portoBox: {
        margin: 17,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    gitText: {
        fontSize: 16,
        fontWeight: 'bold',
        lineHeight: 19,
        textAlignVertical: 'center',
        textAlign: 'center',
    },
    accountText: {
        fontSize: 16,
        fontWeight: 'bold',
        lineHeight: 19,
        textAlignVertical: 'center',
        textAlign: 'left',
        flex: 2
    },
    contactMe: {
        width: Dimensions.get('window').width - 8,
        height: 'auto',
        backgroundColor: "#EFEFEF",
        borderRadius: 16,
        marginTop: 9
    },
    contactMeBox: {
        margin: 17,
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    contactElement: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingHorizontal: 30,
    },
    socialIcon: {
        flex: 1,
        paddingLeft: 50
    }
})