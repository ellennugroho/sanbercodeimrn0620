// Soal 1

console.log("==================================================");
console.log("First answer");
console.log("--------------------------------------------------");

function range(start, end) {
    if (!start || !end) {
        return -1;
    } else {
        result = [];
        if (start < end) {
            for (var i = start; i <= end; i++) {
                result.push(i);
            }
        } else {
            for (var i = start; i >= end; i--) {
                result.push(i);
            }
        }
        return result;
    }
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1 


// Soal 2
// Asumsi: bila param 1 atau 2 tidak diisi, return -1

console.log("==================================================");
console.log("Second answer");
console.log("--------------------------------------------------");

function rangeWithStep(start, end, step = 1) {
    if (!start || !end) {
        return -1;
    } else {
        result = [];
        if (start < end) {
            for (var i = start; i <= end; i += step) {
                result.push(i);
            }
        } else {
            for (var i = start; i >= end; i -= step) {
                result.push(i);
            }
        }
        return result;
    }
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 


// Soal 3

console.log("==================================================");
console.log("Third answer");
console.log("--------------------------------------------------");

function sum(start, end, step = 1) {
    if (!start) {
        return 0;
    } else if (!end) {
        return start;
    } else {
        total = 0
        if (start < end) {
            for (var i = start; i <= end; i += step) {
                total += i;
            }
        } else {
            for (var i = start; i >= end; i -= step) {
                total += i;
            }
        }
        return total;
    }
}

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 


// Soal 4

console.log("==================================================");
console.log("Fourth answer");
console.log("--------------------------------------------------");

function dataHandling(input) {
    for (const data of input) {
        console.log("Nomor ID: " + String(data[0]));
        console.log("Nama Lengkap: " + String(data[1]));
        console.log("TTL: " + String(data[2]) + " " + String(data[3]));
        console.log("Hobi: " + String(data[4]) + "\n");
    }

    return;
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

dataHandling(input);


// Soal 5

console.log("==================================================");
console.log("Fifth answer");
console.log("--------------------------------------------------");

function balikKata(sentence) {
    return sentence.split("").reverse().join("");
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 


// Soal 6

console.log("==================================================");
console.log("Sixth answer");
console.log("--------------------------------------------------");

function dataHandling2(input) {
    var temp = input
    var nama = temp[1] + "Elsharawy";
    var provinsi = "Provinsi " + temp[2];
    var tanggalLahir = temp[3];
    temp.splice(1, 1, nama);
    temp.splice(2, 1, provinsi);
    temp.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(temp);

    var tanggal = tanggalLahir.split("/");
    var bulanString;

    switch (tanggal[1]) {
        case "01": { bulanString = "Januari"; break; }
        case "02": { bulanString = "Februari"; break; }
        case "03": { bulanString = "Maret"; break; }
        case "04": { bulanString = "April"; break; }
        case "05": { bulanString = "Mei"; break; }
        case "06": { bulanString = "Juni"; break; }
        case "07": { bulanString = "Juli"; break; }
        case "08": { bulanString = "Agustus"; break; }
        case "09": { bulanString = "September"; break; }
        case "10": { bulanString = "Oktober"; break; }
        case "11": { bulanString = "November"; break; }
        case "12": { bulanString = "Desember"; break; }
        default: {  }
    }

    console.log(bulanString);
    
    tanggalToSort = tanggal.map(Number);
    tanggalToSort.sort();
    newTanggal = [];
    for (const tanggalan of tanggalToSort) {
        if (tanggalan < 10) {
            newTanggal.push('0' + String(tanggalan));
        } else {
            newTanggal.push(String(tanggalan));
        }
    }
    console.log(newTanggal);
    console.log(tanggalLahir.split("/").join("-"));
    console.log(nama.slice(0,15));

    return input;
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

console.log("=======================END========================");